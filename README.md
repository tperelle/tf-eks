# Terraform EKS example

## Description

Example project to create EKS cluster on AWS with Terraform.

## Prerequisites

- Export AWS credentials in environment variables
- aws Cli installed
- aws-iam-authenticator installed

## Usage

Create AWS resources:

```bash
terraform init
terraform plan
terraform apply
```

## Access EKS cluster

Add the cluster config output to your config file, usually in `~/.kube/config` or a dedicated config file.

```bash
aws eks update-kubeconfig --name terraform-eks-demo
```

Change current context if necessary:

```bash
kubectl config use-context kubernetes
```

Access the cluster:

```bash
kubectl get nodes
kubectl get svc
```